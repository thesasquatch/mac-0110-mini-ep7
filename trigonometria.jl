# MAC0110 - MiniEP7
# Guilherme Simões Santos Marin - 10758748












































function quaseigual(v1, v2)
    erro = 0.0001
    igual = abs(v1 - v2)
    if igual <= erro
        return true
    else
        return false
    end
end

function check_sin(value, x)
    x_fraction = (x / (2 * π)) - (x ÷ (2 * π))
    first_cycle_arc = x_fraction * (2 * π)
    return quaseigual(value, sin(first_cycle_arc))
end

function check_cos(value, x)
    x_fraction = (x / (2 * π)) - (x ÷ (2 * π)) 
    first_cycle_arc = x_fraction * (2 * π)
    return quaseigual(value, cos(first_cycle_arc))
end

function check_tan(value, x)
    x_fraction = (x / (2 * π)) - (x ÷ (2 * π)) 
    first_cycle_arc = x_fraction * (2 * π)
    if abs(first_cycle_arc) == (π / 2) || abs(first_cycle_arc) == (3 * π) / 2
        return false
    elseif abs(first_cycle_arc) < (π / 2)
        return quaseigual(value, tan(first_cycle_arc))
    elseif first_cycle_arc > 0
        return quaseigual(value, tan(first_cycle_arc - π))
    elseif first_cycle_arc < 0
        return quaseigual(value, tan(first_cycle_arc + π))
    end
end


function taylor_sin(x)
    sum = 0
    for i in 0:10
        term = ((-1) ^ i) * (BigFloat(x) ^ (2 * i + 1)) / factorial(BigInt(2 * i + 1))
        sum = sum + term
    end
    return sum
end
        
function taylor_cos(x)
    sum = 0
    for i in 0:12
        term = ((-1) ^ i) * (BigFloat(x) ^ (2 * i)) / factorial(BigInt(2 * i))
        sum = sum + term
    end
    return sum
end

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0 : n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function taylor_tan(x)
    if abs(x) < (π / 2)
        sum = 0.0
        for i in 1:130
            term = (2.0 ^ (2i)) * ((BigFloat(2.000) ^ (2 * i)) - 1.0) * BigFloat(bernoulli(i)) *  BigFloat(x) ^ ((2.0 * i) - 1.0)  / factorial(BigInt(2 * i))
            sum = sum + term
        end
        return sum
    end
end


function test()
    if !quaseigual(1, sin(π / 2))
        println("A função sin(x) retornou o valor errado para x = π / 2")
    end
    if !quaseigual(-0.8660254, sin(-π / 3))
        println("A função sin(x) retornou o valor errado para x = -π / 3")
    end
    if !quaseigual(0, sin(0))
        println("A função sin(x) retornou o valor errado para x = 0")
    end
    if !quaseigual(7.1153697223, tan(1.43117))
        println("A função tan(x) retornou o valor errado para x = 1.43117")
    end
    if !quaseigual(-1, tan(-π / 4))
        println("A função tan(x) retornou o valor errado para x = -π / 4")
    end
    if !quaseigual(0, tan(0))
        println("A função tan(x) retornou o valor errado para x = 0")
    end
    if !check_cos(-1 / 2, 2 * π / 3)
        println("A função check_cos retornou o valor de cosseno errado para x = 2 * π / 3")
    end
    if !check_cos(-1, π)
        println("A função check_cos retornou o valor de cosseno errado para x = π")
    end
    if !check_cos((2 ^ 0.5) / 2, -π / 4)
        println("A função check_cos retornou o valor de cosseno errado para x = -π / 4")
    end
    if !check_tan(-1.7320508, -π / 3)
        println("A função check_tan retornou o valor de tangente errado para x = -π / 3")
    end
    if !check_tan(0.726542, π / 5)
        println("A função check_tan retornou o valor de tangente errado para x = π / 5")
    end
    if !check_tan(-1, 3 * π / 4)
        println("A função check_tan retornou o valor de tangente errado para x = 3 * π / 4")
    end
    if !check_sin(-1 / 2, 7 * π / 6)
        println("A função check_sin retornou o valor de seno errado para x = 7 * π / 6")
    end
    if !check_sin(-1, 3 * π / 2)
        println("A função check_sin retornou o valor de seno errado para x = 3 * π / 2")
    end
    if !check_sin(-1, - π / 2)
        println("A função check_sin retornou o valor de seno errado para x = - π / 2")
    end
    if !quaseigual((3 ^ 0.5) / 2, cos(π / 6))
        println("A função cos(x) retornou o valor errado para x = π / 6")
    end
    if !quaseigual(-(2 ^ 0.5) / 2, cos(-3 * π / 4))
        println("A função cos(x) retornou o valor errado para x = -3 * π / 4")
    end
    if !quaseigual(1, cos(0))
        println("A função cos(x) retornou o valor errado para x = 0")
    end
    println("Fim dos testes")
end




